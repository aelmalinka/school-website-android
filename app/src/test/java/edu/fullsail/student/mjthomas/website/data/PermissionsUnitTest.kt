package edu.fullsail.student.mjthomas.website.data

import com.nhaarman.mockitokotlin2.*
import org.json.JSONObject
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PermissionsUnitTest {
	@Test
	fun testHttpPermissions_fetch() {
		HttpPermissions.factory = mock {
			on {
				getJson(anyString(), any(), any(), any())
			} doReturn(MockRequest())
		}
		val pages: JSONObject = mock {
			on {
				keys()
			} doReturn(listOf("add", "edit", "remove").iterator())
			on {
				getBoolean(anyString())
			} doReturn(true)
		}
		val users: JSONObject = mock {
			on {
				keys()
			} doReturn(listOf("add", "edit", "remove").iterator())
			on {
				getBoolean(anyString())
			} doReturn(false)
		}
		val perms: JSONObject = mock {
			on {
				keys()
			} doReturn(listOf("pages", "users").iterator())
			on {
				getJSONObject("pages")
			} doReturn(pages)
			on {
				getJSONObject("users")
			} doReturn(users)
		}
		val res: JSONObject = mock {
			on {
				getJSONObject("permissions")
			} doReturn(perms)
		}

		var ran = false
		val permissions = HttpPermissions()
		permissions.fetch {
			ran = true
		}

		val cb = argumentCaptor<(JSONObject) -> Unit>()
		verify(HttpPermissions.factory).getJson(anyString(), cb.capture(), any(), any())

		cb.lastValue(res)

		assert(ran)
		assert(permissions.getValue("pages").getValue("add"))
		assert(permissions.getValue("pages").getValue("edit"))
		assert(permissions.getValue("pages").getValue("remove"))
		assertEquals(false, permissions.getValue("users").getValue("add"))
		assertEquals(false, permissions.getValue("users").getValue("edit"))
		assertEquals(false, permissions.getValue("users").getValue("remove"))
	}
}
