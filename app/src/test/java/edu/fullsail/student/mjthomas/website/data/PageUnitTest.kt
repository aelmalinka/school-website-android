package edu.fullsail.student.mjthomas.website.data

import com.nhaarman.mockitokotlin2.*
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PageUnitTest {
	@Test
	fun testHttpPage_toString() {
		val page = HttpPage("Home")
		assertEquals("Home", page.name)
		assertEquals("Home", page.toString())
	}
	@Test
	fun testHttpPage_body() {
		val page = HttpPage("Home")
		assertEquals("", page.body)
	}
	@Test
	fun testHttpPage_fetch() {
		HttpPage.factory = mock {
			on {
				getString(anyString(), any(), any())
			} doReturn(MockRequest())
		}

		val page = HttpPage("test")
		page.fetch {}

		val cb = argumentCaptor<(String) -> Unit>()

		verify(HttpPage.factory).getString(eq("test"), cb.capture(), any())

		cb.lastValue("This is a test page")
		assertEquals("This is a test page", page.body)
	}
	@Test
	fun testHttpPage_save() {
		HttpPage.factory = mock {
			on {
				postString(anyString(), anyOrNull(), any(), any())
			} doReturn(MockRequest())
		}

		var called = false
		val page = HttpPage("test")
		page.body = "This was a test page"
		page.save {
			called = true
		}

		val cb = argumentCaptor<(String) -> Unit>()
		verify(HttpPage.factory).postString(eq("test"), anyOrNull(), cb.capture(), any())
		cb.lastValue("")

		assert(called)
	}
	@Test
	fun testHttpPage_remove() {
		HttpPage.factory = mock {
			on {
				delete(anyString(), any(), any())
			} doReturn(MockRequest())
		}

		var called = false
		val page = HttpPage("test")
		page.remove {
			called = true
		}

		val cb = argumentCaptor<(String) -> Unit>()
		verify(HttpPage.factory).delete(eq("test"), cb.capture(), any())
		cb.lastValue("")

		assert(called)
	}
	@Test
	fun testHttpPage_cancel() {
		val page = HttpPage("test")
		page.cancel()
	}
}
