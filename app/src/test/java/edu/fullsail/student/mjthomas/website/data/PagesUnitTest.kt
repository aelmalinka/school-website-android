package edu.fullsail.student.mjthomas.website.data

import com.nhaarman.mockitokotlin2.*
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PagesUnitTest {
	@Test
	fun testHttpPages_cancel() {
		val pages = HttpPages()
		pages.cancel()
	}
	@Test
	fun testHttpPages_fetch() {
		HttpPages.factory = mock {
			on {
				getJsonArray(anyString(), any(), any())
			} doReturn(MockRequest())
		}
		val page = mock<JSONObject> {
			on {
				getString("name")
			} doReturn("Test Page")
		}
		val res = mock<JSONArray> {
			on {
				getJSONObject(anyInt())
			} doReturn(page)
			on {
				length()
			} doReturn(5)
		}

		val it = HttpPages()
		it.fetch {}

		val cb = argumentCaptor<(JSONArray) -> Unit>()
		verify(HttpPages.factory).getJsonArray(eq(""), cb.capture(), any())
		cb.lastValue(res)

		verify(res).length()
		verify(res, times(5)).getJSONObject(anyInt())
		verify(page, times(5)).getString("name")

		assertEquals(5, it.size)
		assertEquals(HttpPage("Test Page").name, it[3].name)

		for(p in it) {
			assertEquals(false, p.name.isEmpty())
			assertEquals(true, p.body.isEmpty())
		}
	}
	@Test
	fun testHttpPages_add() {
		HttpPages.factory = mock {
			on {
				putString(anyString(), any(), any(), any())
			} doReturn(MockRequest())
		}

		val pages = HttpPages()
		val page = HttpPage("test")
		var ran = false

		pages.add(page) {
			ran = true
			assertEquals(page.name, it.name)
		}

		val cb = argumentCaptor<(String) -> Unit>()
		verify(HttpPages.factory).putString(eq(page.name), any(), cb.capture(), any())
		cb.lastValue(page.name)

		assert(ran)
	}
}
