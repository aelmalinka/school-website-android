package edu.fullsail.student.mjthomas.website.data

import com.nhaarman.mockitokotlin2.*
import org.json.JSONObject
import org.junit.Assert.assertEquals
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UsersUnitTest {
	@Test
	fun testHttpUser_login() {
		HttpUser.factory = mock {
			on {
				postJsonJson(eq("login"), any(), any(), any())
			} doReturn(MockRequest())
		}
		val res : JSONObject = mock {
			on {
				getString(eq("key"))
			} doReturn("")
		}

		val user = HttpUser("aladdin")
		user.login("open sesame") {}

		val cb = argumentCaptor<(JSONObject) -> Unit>()
		verify(HttpUser.factory).postJsonJson(eq("login"), any(), cb.capture(), any())
		cb.lastValue(res)
		verify(res).getString(eq("key"))
	}
	@Test
	fun testHttpUser_logout() {
		HttpUser.factory = mock {
			on {
				getString(eq("logout"), any(), any())
			} doReturn(MockRequest())
		}

		var called = false
		val user = HttpUser("aladdin", "I am logged in")
		user.logout {
			called = true
		}

		val cb = argumentCaptor<(String) -> Unit>()
		verify(HttpUser.factory).getString(eq("logout"), cb.capture(), any())
		cb.lastValue("")

		assertEquals(true, called)
	}
	@Test
	@Ignore("JSONObject() created in save method")	//	2019-07-19 AMR TODO
	fun testHttpUser_save() {
		HttpUser.factory = mock {
			on {
				postJson(eq("aladdin"), any(), any(), any())
			} doReturn(MockRequest())
		}

		var called = false
		val user = HttpUser("aladdin")
		user.save {
			called = true
		}

		val cb = argumentCaptor<(String) -> Unit>()
		verify(HttpUser.factory).postJson(eq("aladdin"), any(), cb.capture(), any())
		cb.lastValue("")

		assert(called)
	}
	@Test
	fun testHttpUser_remove() {
		HttpUser.factory = mock {
			on {
				delete(eq("aladdin"), any(), any())
			} doReturn(MockRequest())
		}

		var called = false
		val user = HttpUser("aladdin")
		user.remove {
			called = true
		}

		val cb = argumentCaptor<(String) -> Unit>()
		verify(HttpUser.factory).delete(eq("aladdin"), cb.capture(), any())
		cb.lastValue("")

		assert(called)
	}
}
