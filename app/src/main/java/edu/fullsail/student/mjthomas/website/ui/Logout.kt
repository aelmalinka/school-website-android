package edu.fullsail.student.mjthomas.website.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import edu.fullsail.student.mjthomas.website.model.UserViewModel

class Logout : Fragment() {
	private var listener : OnLoggedOut? = null
	private val model : UserViewModel
		get() = activity?.run {
			ViewModelProviders.of(this).get(UserViewModel::class.java)
		} ?: throw Error("Invalid Activity")
	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		model.logout {
			listener?.onLoggedOut()
		}

		return null
	}
	override fun onAttach(context: Context?) {
		super.onAttach(context)
		when(context) {
			is OnLoggedOut -> listener = context
			else -> throw Error("$context must implement OnLoggedIn")
		}
	}
	override fun onDetach() {
		super.onDetach()
		listener = null
	}
	interface OnLoggedOut {
		fun onLoggedOut()
	}
	companion object {
		@JvmStatic
		fun newInstance() : Logout =
			Logout()
	}
}
