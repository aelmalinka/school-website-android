package edu.fullsail.student.mjthomas.website.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.android.volley.AuthFailureError
import com.android.volley.VolleyError
import com.android.volley.toolbox.Volley
import com.google.android.material.navigation.NavigationView
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.data.*
import edu.fullsail.student.mjthomas.website.model.PageViewModel
import edu.fullsail.student.mjthomas.website.model.PagesViewModel
import edu.fullsail.student.mjthomas.website.model.UserViewModel
import org.commonmark.node.SoftLineBreak
import ru.noties.markwon.AbstractMarkwonPlugin
import ru.noties.markwon.Markwon
import ru.noties.markwon.MarkwonVisitor
import ru.noties.markwon.ext.tasklist.TaskListPlugin

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
	private val menuDrawer: NavigationView
		get() = findViewById(R.id.menu_drawer)
	private val drawerLayout: DrawerLayout
		get() = findViewById(R.id.drawer_layout)
	private val main: TextView
		get() = findViewById(R.id.main)
	private val pages: Pages
		get() = ViewModelProviders.of(this).get(PagesViewModel::class.java).pages
	private var user: User?
		get() = ViewModelProviders.of(this).get(UserViewModel::class.java).user.value
		set(value) {
			ViewModelProviders.of(this).get(UserViewModel::class.java).setUser(value)
		}
	private var defPerms: Map<String, Map<String, Boolean>>? = null
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		menuDrawer.setNavigationItemSelectedListener(this)
		RequestFactory.queue = Volley.newRequestQueue(this)
		ViewModelProviders.of(this).get(UserViewModel::class.java).errorCallback = {
			when(it) {
				is Error -> Toast.makeText(this, "Error: ${it.message}", Toast.LENGTH_SHORT).show()
				is VolleyError -> Toast.makeText(this, "VolleyError: $it", Toast.LENGTH_SHORT).show()
				else -> Toast.makeText(this, "Unknown Error: $it", Toast.LENGTH_SHORT).show()
			}
			when(it) {
				is AuthFailureError -> user = null
			}
		}
		ViewModelProviders.of(this).get(UserViewModel::class.java).user.observe(this, Observer<User> {
			RequestFactory.user = it
			it?.fetch {}
		})
		getPage("Home")
		HttpPermissions.defaultPermissions {
			defPerms = it
		}
	}
	override fun onBackPressed() {
		if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
			drawerLayout.closeDrawer(GravityCompat.END)
		} else {
			super.onBackPressed()
		}
	}
	override fun onNavigationItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			R.id.menu_user_login -> when(user?.isLoggedIn) {
				true -> startActivityForResult(
					Intent(this, LoginLogoutActivity::class.java).apply {
						putExtra(User.PARCEL, user)
					},
					LOGIN_REQUEST_CODE
				)
				else -> startActivityForResult(
					Intent(this, LoginLogoutActivity::class.java),
					LOGIN_REQUEST_CODE
				)
			}
			R.id.menu_ip -> editServerIP()
			R.id.menu_page_add -> editPage(Action.NEW)
			R.id.menu_page_edit -> editPage(Action.EDIT)
			R.id.menu_page_delete -> editPage(Action.DELETE)
			R.id.menu_user_add -> editUser(Action.NEW)
			R.id.menu_user_edit -> editUser(Action.EDIT)
			R.id.menu_user_delete -> editUser(Action.DELETE)
		}
		drawerLayout.closeDrawer(GravityCompat.END)
		return true
	}
	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		when (requestCode) {
			LOGIN_REQUEST_CODE -> when (resultCode) {
				RESULT_OK ->
					user = when (data?.hasExtra(User.PARCEL)) {
						true -> data.getParcelableExtra(User.PARCEL)
						else -> null
					}
				RESULT_CANCELED -> {}
			}
			SERVER_REQUEST_CODE -> {
				getPage("Home")
			}
			else -> Log.e(TAG, "Invalid request code in ActivityResult")
		}
	}
	fun onMenu(@Suppress("UNUSED_PARAMETER") view: View) {
		menuDrawer.menu.clear()
		menuInflater.inflate(R.menu.activity_main_drawer_menu, menuDrawer.menu)
		menuDrawer.menu.findItem(R.id.menu_user_login).title = when(user?.isLoggedIn) {
			true -> "Logout"
			else -> "Login"
		}
		menuDrawer.menu.findItem(R.id.menu_page_add).isEnabled = checkPerm("pages", "add")
		menuDrawer.menu.findItem(R.id.menu_page_edit).isEnabled = checkPerm("pages", "edit")
		menuDrawer.menu.findItem(R.id.menu_page_delete).isEnabled = checkPerm("pages", "remove")
		menuDrawer.menu.findItem(R.id.menu_user_add).isEnabled = checkPerm("users", "add")
		menuDrawer.menu.findItem(R.id.menu_user_edit).isEnabled = checkPerm("users", "edit")
		menuDrawer.menu.findItem(R.id.menu_user_delete).isEnabled = checkPerm("users", "remove")
		drawerLayout.openDrawer(GravityCompat.END)
	}
	private fun checkPerm(app: String, perm: String) : Boolean = when(user?.isLoggedIn) {
		true -> user?.permissions?.get(app)?.get(perm) ?: defPerms?.get(app)?.get(perm) ?: false
		else -> defPerms?.get(app)?.get(perm) ?: false
	}
	fun onNav(@Suppress("UNUSED_PARAMETER") view: View) {
		menuDrawer.menu.clear()
		pages.fetch {
			for(page in it)
				menuDrawer.menu.add(page.name).setOnMenuItemClickListener {
					onNavClick(page)
					true
				}
		}
		drawerLayout.openDrawer(GravityCompat.END)
	}
	private fun getPage(name: String) {
		val model = ViewModelProviders.of(this).get(PageViewModel::class.java)
		model.getPage(name).observe(this, Observer<Page> {
			md.setMarkdown(main, it.body)
		})
	}
	private fun onNavClick(page: Page) {
		page.fetch {
			md.setMarkdown(main, it.body)
		}
		drawerLayout.closeDrawer(GravityCompat.END)
	}
	private fun editPage(action: Action) =
		startActivity(Intent(this, EditPageActivity::class.java).apply {
			putExtra(Pages.PARCEL, pages)
			putExtra(Action.PARCEL, action)
		})
	private fun editUser(action: Action) =
		startActivity(Intent(this, EditUserActivity::class.java).apply {
			putExtra(Action.PARCEL, action)
			if(user != null)
				putExtra(User.PARCEL, user)
		})
	private fun editServerIP() =
		startActivityForResult(
			Intent(this, SetServerIP::class.java),
			SERVER_REQUEST_CODE
		)
	private var _md: Markwon? = null
	private val md: Markwon
		get() {
			if(_md == null)
				_md = Markwon.builder(this)
					.usePlugin(object : AbstractMarkwonPlugin() {
						override fun configureVisitor(builder: MarkwonVisitor.Builder) {
							builder.on(SoftLineBreak::class.java) { visitor, _ -> visitor.forceNewLine() }
						}
					})
					.usePlugin(TaskListPlugin.create(this))
					.build()

			return _md!!
		}
	companion object {
		const val LOGIN_REQUEST_CODE = 0
		const val SERVER_REQUEST_CODE = 1
		const val TAG = "Website"
	}
}
