package edu.fullsail.student.mjthomas.website.ui

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import edu.fullsail.student.mjthomas.website.data.HttpPermissions
import edu.fullsail.student.mjthomas.website.data.Permissions as dataPermissions

class Permissions(
	ctx: Context,
	attrs: AttributeSet? = null
) :
	LinearLayout(ctx, attrs),
	dataPermissions
{
	private val checks = mutableMapOf<String, MutableMap<String, CheckBox>>()
	private val map : Map<String, Map<String, Boolean>>
		get() = checks.map {(a, i) ->
			a to i.map{(n, c) ->
				n to c.isChecked
			}.toMap()
		}.toMap()
	private lateinit var default : Map<String, Map<String, Boolean>>
	private lateinit var all : Map<String, List<String>>
	init {
		HttpPermissions.defaultPermissions { def ->
			default = def
			HttpPermissions.allPermissions { a ->
				all = a
				Log.d(TAG, "Constructing Checks")
				for((app, i) in all) {
					Log.d(TAG, "Adding app $a")
					checks[app] = mutableMapOf()
					for(n in i) {
						Log.d(TAG, "Adding perm $n")
						checks[app]!![n] = CheckBox(context).apply {
							text = n
							isChecked = default[app]?.get(n) ?: false
						}
					}
				}
				Log.d(TAG, "Constructing visuals")
				for((app, i) in checks) {
					addView(TextView(context).apply {
						text = app
					})
					for((_, c) in i) {
						addView(c)
					}
				}
			}
		}
	}
	companion object {
		const val TAG = "Website/UI/Permission"
	}
	private fun getMap(app: String) = when(checks.contains(app)) {
		false -> {
			checks[app] = mutableMapOf()
			checks[app]!!
		}
		true -> checks[app]!!
	}
	private fun getCheckBox(app: String, name: String) = when(getMap(app).contains(name)) {
		false -> {
			getMap(app)[name] = CheckBox(context).apply {
				text = name
				isChecked = false
			}
			getMap(app)[name]!!
		}
		else -> getMap(app)[name]!!
	}
	override var errorCallback: (Any?) -> Unit = {}
	override val isModified: Boolean
		get() = map.none { (a, i) ->
			i.none { (n, v) ->
				default[a]?.get(n) ?: false != v
			}
		}
	override fun fetch(f: (dataPermissions) -> Unit) {
		f(this)
	}
	override fun cancel() {}
	override fun set(app: String, name: String, value: Boolean) {
		getCheckBox(app, name).isChecked = value
	}
	override val entries: Set<Map.Entry<String, Map<String, Boolean>>>
		get() = map.entries
	override val values: Collection<Map<String, Boolean>>
		get() = map.values
	override val keys: Set<String>
		get() = map.keys
	override val size: Int
		get() = map.size
	override fun containsKey(key: String): Boolean = map.containsKey(key)
	override fun containsValue(value: Map<String, Boolean>): Boolean = map.containsValue(value)
	override fun get(key: String): Map<String, Boolean>? = map[key]
	override fun isEmpty(): Boolean = map.isEmpty()
}
