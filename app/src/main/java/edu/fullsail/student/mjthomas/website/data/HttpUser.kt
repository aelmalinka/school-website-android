package edu.fullsail.student.mjthomas.website.data

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.android.volley.VolleyError
import org.json.JSONObject

class HttpUser(username: String, key : String? = null) : User {
	private var oldName : String? = null
	private var _modified : Boolean = false
	override var name : String = username
		set(value) {
			if(value != field && value != "") {
				if (oldName == null)
					oldName = name
				field = value
			}
		}
	override var permissions : Permissions = HttpPermissions()
		set(value) {
			_modified = true
			field = value.also {
				it.errorCallback = errorCallback
			}
		}
	override var key : String? = key
		private set
	override var errorCallback: (Any?) -> Unit = {}
		set(value) {
			permissions.errorCallback = value
			field = value
		}
	override val isLoggedIn : Boolean
		get() = key != null
	private var _password : String? = null
	override fun authError(it: Any?) {
		key = null
		errorCallback(it)
	}
	override fun cancel() {
		permissions.cancel()
		loginReq?.cancel()
		logoutReq?.cancel()
		saveReq?.cancel()
		delReq?.cancel()
	}
	override fun login(password: String, f: (User) -> Unit) {
		loginReq?.cancel()
		loginReq = factory.postJsonJson(
			"login",
			JSONObject("{\"name\": \"$name\", \"pass\": \"$password\"}"), {
				key = it.getString("key")
				f(this)
			}, {
				error(it)
			}
		)
	}
	override fun logout(f: () -> Unit) {
		logoutReq?.cancel()
		if(key != null) {
			logoutReq = factory.getString(
				"logout", {
					key = null
					f()
				}, {
					error(it)
				}
			)
		}
	}
	override fun setPassword(password: String) {
		if(password != "")
			_password = password
	}
	override fun fetch(f: (User) -> Unit) {
		permissions.cancel()
		permissions.fetch {
			f(this)
		}
	}
	override fun save(f: (User) -> Unit) {
		val json = JSONObject()
		if(oldName != null)
			json.put("name", name)
		json.put("pass", _password)
		if(_modified || permissions.isModified)
			json.put("permissions", perms(permissions))

		saveReq?.cancel()
		saveReq = factory.postJson(oldName ?: name, json, {
			_password = null
			oldName = null
			if(_modified) {
				permissions = HttpPermissions()
				_modified = false
				permissions.cancel()
				permissions.fetch {
					f(this)
				}
			} else f(this)
		}, {
			error(it)
		})
	}
	override fun remove(f: (User) -> Unit) {
		delReq?.cancel()
		delReq = factory.delete(name, {
			f(this)
		}, {
			error(it)
		})
	}
	override fun describeContents(): Int = 0
	override fun writeToParcel(dest: Parcel?, flags: Int) {
		dest?.writeStringList(listOf(
			name,
			key
		))
	}
	private var loginReq: Request? = null
	private var logoutReq: Request? = null
	private var saveReq: Request? = null
	private var delReq: Request? = null
	companion object CREATOR : Parcelable.Creator<User> {
		override fun createFromParcel(source: Parcel?): User {
			val data = mutableListOf<String?>(null, null)
			source?.readStringList(data)
			val (name, pass) = data
			if(data.size < 1 || data.size > 2)
				throw Error("Invalid Parcel")
			return HttpUser(name?:throw Error("Invalid Parcel"), pass)
		}
		override fun newArray(size: Int): Array<User?> =
			arrayOfNulls(size)
		fun add(username: String, password: String, permissions: Map<String, Map<String, Boolean>>, f: (User) -> Unit) : Request {
			val json = JSONObject()
			json.put("name", username)
			json.put("pass", password)
			json.put("permissions", perms(permissions))
			return factory.putJson("", json, {
				f(HttpUser(it))
			}, {
				error(it)
			})
		}
		fun get(f: (List<User>) -> Unit) {
			factory.getJsonArray("all", {
				f(arrayOfNulls<User>(it.length()).apply {
					for (i in 0 until it.length()) {
						set(i, HttpUser(it.getJSONObject(i).getString("name")))
					}
				}.filterNotNull().toList())
			}, {
				error(it)
			})
		}
		var factory: RequestFactory = RequestFactory("8081")
		var error: (Any?) -> Unit = {}
		private const val TAG = "Website/User"
		private fun perms(permissions: Map<String, Map<String, Boolean>>) = JSONObject().apply {
			for((k, v) in permissions) {
				val perm = JSONObject()
				for((key, value) in v) {
					perm.put(key, value)
				}
				put(k, perm)
			}
		}
		private fun _error(it: Any?) {
			when(it) {
				is Error -> Log.e(TAG, it.message, it)
				is VolleyError -> Log.e(TAG, it.message, it)
				else -> Log.e(TAG, "$it")
			}
			error(it)
		}
	}
	private fun error(it: Any?) {
		_error(it)
		errorCallback(it)
	}
}
