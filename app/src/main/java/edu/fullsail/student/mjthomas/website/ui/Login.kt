package edu.fullsail.student.mjthomas.website.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.model.UserViewModel

class Login : Fragment() {
	private var listener: OnLoggedIn? = null
	private val model: UserViewModel
		get() = activity?.run {
			ViewModelProviders.of(this).get(UserViewModel::class.java)
		} ?: throw Error("Invalid Activity")
	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? =
		inflater.inflate(
			R.layout.fragment_login,
			container,
			false
		).apply {
			val login = findViewById<Button>(R.id.login)
			val cancel = findViewById<Button>(R.id.loginCancel)

			login.setOnClickListener {
				val name = findViewById<TextView>(R.id.username).text.toString()
				val pass = findViewById<TextView>(R.id.password).text.toString()
				model.login(name, pass) {
					if(model.isLoggedIn)
						listener?.onLoggedIn()
				}
			}
			cancel.setOnClickListener {
				listener?.onCancel()
			}
		}
	override fun onAttach(context: Context?) {
		super.onAttach(context)
		when(context) {
			is OnLoggedIn -> listener = context
			else -> throw Error("$context must implement OnLoggedIn")
		}
	}
	override fun onDetach() {
		super.onDetach()
		listener = null
	}
	interface OnLoggedIn {
		fun onLoggedIn()
		fun onCancel()
	}
	companion object {
		@JvmStatic
		fun newInstance() : Login =
			Login()
	}
}
