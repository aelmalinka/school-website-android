package edu.fullsail.student.mjthomas.website.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.data.HttpUser
import edu.fullsail.student.mjthomas.website.data.User

class SelectUser : Fragment() {
	private var listener: OnUserSelectedListener? = null
	private var canList : Boolean = false
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		arguments?.apply {
			canList = getBoolean(CAN_LIST)
		}
		HttpUser.error = errorCallback
	}
	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? =
		when(canList) {
			true -> inflater.inflate(R.layout.fragment_user_list, container, false).apply {
				with(findViewById<RecyclerView>(R.id.list)) {
					HttpUser.get {
						layoutManager = LinearLayoutManager(context)
						adapter = SelectUserAdapter(it, listener)
					}
				}
				findViewById<Button>(R.id.cancel).setOnClickListener {
					listener?.onCancel()
				}
			}
			else -> inflater.inflate(R.layout.fragment_user_no_list, container, false).apply {
				findViewById<Button>(R.id.ok).setOnClickListener {
					listener?.onUserSelected(HttpUser(findViewById<EditText>(R.id.username).text.toString()).also {
						it.errorCallback = errorCallback
					})
				}
				findViewById<Button>(R.id.cancel).setOnClickListener {
					listener?.onCancel()
				}
			}
		}
	override fun onAttach(context: Context?) {
		super.onAttach(context)
		if(context is OnUserSelectedListener) {
			listener = context
			HttpUser.error = {
				listener?.onError(it)
			}
		} else {
			throw RuntimeException("$context must implement OnUserSelectedListener")
		}
	}
	override fun onDetach() {
		super.onDetach()
		listener = null
	}
	interface OnUserSelectedListener {
		fun onUserSelected(user: User)
		fun onCancel()
		fun onError(it: Any?)
	}
	companion object {
		const val CAN_LIST = "edu.fullsail.student.mjthomas.website.ui.SelectUser.canList"
		var errorCallback: (Any?) -> Unit = {}
		@JvmStatic
		fun newInstance(canList: Boolean) =
			SelectUser().apply {
				arguments = Bundle().apply {
					putBoolean(CAN_LIST, canList)
				}
			}
	}
}
