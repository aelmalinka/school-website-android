package edu.fullsail.student.mjthomas.website.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.data.HttpPage
import edu.fullsail.student.mjthomas.website.data.Page
import edu.fullsail.student.mjthomas.website.data.Pages

class AddPage : Fragment() {
	private var pages: Pages? = null
	private var listener: OnPageAddedListener? = null
	private val name: TextView?
		get() = view?.findViewById(R.id.addName)
	private val body: TextView?
		get() = view?.findViewById(R.id.addBody)
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		arguments?.let {
			pages = it.getParcelable(Pages.PARCEL)
		}
	}
	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		// Inflate the layout for this fragment
		val ret = inflater.inflate(R.layout.fragment_add_page, container, false)

		with(ret) {
			val cancel = findViewById<Button>(R.id.cancel)
			val save = findViewById<Button>(R.id.save)

			cancel.setOnClickListener {
				onCancel(it)
			}
			save.setOnClickListener {
				onSave(it)
			}
		}

		return ret
	}
	override fun onAttach(context: Context) {
		super.onAttach(context)
		if (context is OnPageAddedListener) {
			listener = context
		} else {
			throw RuntimeException("$context must implement OnFragmentInteractionListener")
		}
	}
	override fun onDetach() {
		super.onDetach()
		listener = null
	}
	private fun onCancel(@Suppress("UNUSED_PARAMETER")view: View) {
		listener?.onCancel()
	}
	private fun onSave(@Suppress("UNUSED_PARAMETER")view: View) {
		val page = HttpPage(name?.text.toString())
		page.body = body?.text.toString()
		pages?.add(page) {
			listener?.onPageAdded(it)
		}
	}
	interface OnPageAddedListener {
		fun onPageAdded(page: Page)
		fun onCancel()
	}
	companion object {
		@JvmStatic
		fun newInstance(pages: Pages) =
			AddPage().apply {
				arguments = Bundle().apply {
					putParcelable(Pages.PARCEL, pages)
				}
			}
	}
}
