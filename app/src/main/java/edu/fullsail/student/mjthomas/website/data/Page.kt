package edu.fullsail.student.mjthomas.website.data

import android.os.Parcelable

interface Page : Parcelable {
	var errorCallback: (Any?) -> Unit
	override fun toString(): String
	val name : String
	var body : String
	fun cancel()
	fun fetch(f: (Page) -> Unit)
	fun save(f: (Page) -> Unit)
	fun remove(f: () -> Unit)
	override fun equals(other: Any?): Boolean
	override fun hashCode(): Int
	companion object {
		const val PARCEL = "edu.fullsail.student.mjthomas.website.page"
	}
}
