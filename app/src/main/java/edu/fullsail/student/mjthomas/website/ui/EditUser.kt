package edu.fullsail.student.mjthomas.website.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.data.HttpUser
import edu.fullsail.student.mjthomas.website.data.User

class EditUser : Fragment() {
	private var listener: OnUserChangedListener? = null
	private var user: User? = null
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		arguments?.apply {
			user = getParcelable(User.PARCEL)
		}
	}
	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? =
		inflater.inflate(R.layout.fragment_edit_user, container, false).apply {
			if(user != null) {
				findViewById<EditText>(R.id.username)?.setText(user?.name, TextView.BufferType.EDITABLE)
			}
			findViewById<Button>(R.id.cancel)?.setOnClickListener {
				listener?.onCancel()
			}
			findViewById<Button>(R.id.save)?.setOnClickListener {
				val username = findViewById<EditText>(R.id.username)?.text.toString()
				val password = findViewById<EditText>(R.id.password)?.text.toString()
				val permissions = findViewById<Permissions>(R.id.permissions)

				when(user) {
					null -> HttpUser.add(username, password, permissions) {
						listener?.onUserChanged(it)
					}
					else -> user?.apply {
						name = username
						setPassword(password)
						this.permissions = permissions
						save {
							listener?.onUserChanged(it)
						}
					}
				}
			}
		}
	override fun onAttach(context: Context?) {
		super.onAttach(context)
		if(context is OnUserChangedListener) {
			listener = context
			HttpUser.error = {
				listener?.onError(it)
			}
			user?.errorCallback = {
				listener?.onError(it)
			}
		} else {
			throw RuntimeException("$context must implement OnUserChangedListener")
		}
	}
	override fun onDetach() {
		super.onDetach()
		listener = null
	}
	interface OnUserChangedListener {
		fun onUserChanged(user: User)
		fun onCancel()
		fun onError(it: Any?)
	}
	companion object {
		@JvmStatic
		fun newInstance(user: User? = null) =
			EditUser().apply {
				arguments = Bundle().apply {
					when(user) {
						is User -> putParcelable(User.PARCEL, user)
					}
				}
			}
	}
}
