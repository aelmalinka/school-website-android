package edu.fullsail.student.mjthomas.website.data

import com.android.volley.AuthFailureError
import com.android.volley.RequestQueue
import com.android.volley.VolleyError
import org.json.JSONArray
import org.json.JSONObject
import com.android.volley.Request as VolleyRequest

open class RequestFactory(private val port : String = "8080") {
	open fun getString(
		url: String,
		cb: (String) -> Unit,
		err: (VolleyError) -> Unit
	): Request {
		val ret = requestString(url, VolleyRequest.Method.GET, null, null, cb, err)
		queue?.add(ret)
		return ret
	}
	open fun getJson(
		url: String,
		cb: (JSONObject) -> Unit,
		err: (VolleyError) -> Unit,
		asUser: Boolean = true
	): Request {
		val ret = requestJsonObject(url, VolleyRequest.Method.GET, null, cb, err, asUser)
		queue?.add(ret)
		return ret
	}
	open fun getJsonArray(
		url: String,
		cb: (JSONArray) -> Unit,
		err: (VolleyError) -> Unit
	): Request {
		val ret = requestJsonArray(url, VolleyRequest.Method.GET, cb, err)
		queue?.add(ret)
		return ret
	}
	open fun putString(
		url: String,
		data: String,
		cb: (String) -> Unit,
		err: (VolleyError) -> Unit
	): Request {
		val ret = requestString(url, VolleyRequest.Method.PUT, null, data, cb, err)
		queue?.add(ret)
		return ret
	}
	open fun putJson(
		url: String,
		data: JSONObject,
		cb: (String) -> Unit,
		err: (VolleyError) -> Unit
	): Request {
		val ret = requestString(url, VolleyRequest.Method.PUT, data, null, cb, err)
		queue?.add(ret)
		return ret
	}
	open fun postString(
		url: String,
		data: String,
		cb: (String) -> Unit,
		err: (VolleyError) -> Unit
	): Request {
		val ret = requestString(url, VolleyRequest.Method.POST, null, data, cb, err)
		queue?.add(ret)
		return ret
	}
	open fun postJsonJson(
		url: String,
		data: JSONObject,
		cb: (JSONObject) -> Unit,
		err: (VolleyError) -> Unit
	): Request {
		val ret = requestJsonObject(url, VolleyRequest.Method.POST, data, cb, err)
		queue?.add(ret)
		return ret
	}
	open fun postJson(
		url: String,
		data: JSONObject,
		cb: (String) -> Unit,
		err: (VolleyError) -> Unit
	): Request {
		val ret = requestString(url, VolleyRequest.Method.POST, data, null, cb, err)
		queue?.add(ret)
		return ret
	}
	open fun delete(
		url: String,
		cb: (String) -> Unit,
		err: (VolleyError) -> Unit
	): Request {
		val ret = requestString(url, VolleyRequest.Method.DELETE, null, null, cb, err)
		queue?.add(ret)
		return ret
	}
	private inline fun requestString(
		url: String,
		method: Int,
		data: JSONObject?,
		str: String?,
		crossinline cb: (String) -> Unit,
		crossinline err: (VolleyError) -> Unit,
		asUser: Boolean = true
	): RequestString {
		return RequestString(
			method, "$baseUrl:$port/$url", data, str,
			{
				cb(it)
			},
			{
				when(it) {
					is AuthFailureError -> user?.authError(it)
				}
				err(it)
			},
			if(asUser) user?.name else null,
			if(asUser) user?.key else null
		)
	}
	private inline fun requestJsonObject(
		url: String,
		method: Int,
		data: JSONObject?,
		crossinline cb: (JSONObject) -> Unit,
		crossinline err: (VolleyError) -> Unit,
		asUser: Boolean = true
	): RequestJsonObject {
		return RequestJsonObject(
			method, "$baseUrl:$port/$url", data,
			{
				cb(it)
			},
			{
				when(it) {
					is AuthFailureError -> user?.authError(it)
				}
				err(it)
			},
			if(asUser) user?.name else null,
			if(asUser) user?.key else null
		)
	}
	private inline fun requestJsonArray(
		url: String,
		method: Int,
		crossinline cb: (JSONArray) -> Unit,
		crossinline err: (VolleyError) -> Unit,
		asUser: Boolean = true
	) : RequestJsonArray {
		return RequestJsonArray(
			method, "$baseUrl:$port/$url",
			{
				cb(it)
			},
			{
				when(it) {
					is AuthFailureError -> user?.authError(it)
				}
				err(it)
			},
			if(asUser) user?.name else null,
			if(asUser) user?.key else null
		)
	}
	companion object {
		var queue: RequestQueue? = null
		var user: User? = null
		var baseUrl: String = "http://127.0.0.1"
	}
}
