package edu.fullsail.student.mjthomas.website.model

import android.os.Parcel
import android.os.Parcelable
import androidx.lifecycle.LiveData
import edu.fullsail.student.mjthomas.website.data.HttpPages
import edu.fullsail.student.mjthomas.website.data.Page
import edu.fullsail.student.mjthomas.website.data.Pages

class LivePages(
	private val pages : Pages = HttpPages()
) :
	Pages by pages,
	LiveData<Pages>()
{
	init {
		value = pages
	}
	override fun fetch(f: (Pages) -> Unit) = pages.fetch {
		value = it
		f(this)
	}
	override fun add(page: Page, f: (Page) -> Unit) = pages.add(page) {
		value = pages
		f(it)
	}
	companion object CREATOR : Parcelable.Creator<Pages> {
		override fun createFromParcel(source: Parcel): Pages = HttpPages.createFromParcel(source)
		override fun newArray(size: Int): Array<Pages?> = HttpPages.newArray(size)
	}
}
