package edu.fullsail.student.mjthomas.website.ui

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.VolleyError
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.data.HttpPages
import edu.fullsail.student.mjthomas.website.data.Page
import edu.fullsail.student.mjthomas.website.data.Pages

class EditPageActivity :
	AppCompatActivity(),
	SelectPage.OnPageSelected,
	AddPage.OnPageAddedListener,
	EditPage.OnPageEdited
{
	private var pages: Pages? = null
	private var action: Action? = null
	private var which: SelectPage? = null
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_edit_page)

		action = intent.getSerializableExtra(Action.PARCEL) as Action
		pages = intent.getParcelableExtra(Pages.PARCEL)

		 SelectPage.errorCallback = {
			when(it) {
				is Error -> Toast.makeText(this, "Error: ${it.message}", Toast.LENGTH_SHORT).show()
				is VolleyError -> Toast.makeText(this, "Volley Error: $it", Toast.LENGTH_SHORT).show()
				is String -> Toast.makeText(this, "Error: $it", Toast.LENGTH_SHORT).show()
				else -> Toast.makeText(this, "Unknown Error", Toast.LENGTH_SHORT).show()
			}
			onCancel()
		}
		pages?.errorCallback = SelectPage.errorCallback

		when(action) {
			Action.NEW -> new()
			Action.EDIT -> select()
			Action.DELETE -> select()
			else -> Log.e(TAG, "Invalid Action")
		}
	}
	override fun onPageSelected(page: Page) {
		when(action) {
			Action.EDIT -> edit(page)
			Action.DELETE -> delete(page)
			else ->
				Log.d(TAG, "Page Selected with Invalid Action")
		}
	}
	override fun onPageAdded(page: Page) {
		Toast.makeText(this, "$page Added!", Toast.LENGTH_LONG).show()
		finish()
	}
	override fun onPageEdited(page: Page) {
		Toast.makeText(this, "$page Edited!", Toast.LENGTH_LONG).show()
		finish()
	}
	override fun onCancel() {
		finish()
	}
	private fun select() {
		if (pages != null)
			which = SelectPage.newInstance(pages!!)

		if(which != null) {
			with(supportFragmentManager.beginTransaction()) {
				add(R.id.page_edit_main, which!!)
				commit()
			}
		}
	}
	private fun new() =
		with(supportFragmentManager.beginTransaction()) {
			add(R.id.page_edit_main, AddPage.newInstance(pages ?: HttpPages()))
			commit()
		}
	private fun edit(page: Page) =
		with(supportFragmentManager.beginTransaction()) {
			replace(R.id.page_edit_main, EditPage.newInstance(page))
			addToBackStack(null)
			commit()
		}
	private fun delete(page: Page) =
		page.remove {
			Toast.makeText(this, "$page Deleted!", Toast.LENGTH_LONG).show()
			finish()
		}
	companion object {
		const val TAG = "Website/EditPageActivity"
	}
}
