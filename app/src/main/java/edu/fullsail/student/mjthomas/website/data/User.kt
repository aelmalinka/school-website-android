package edu.fullsail.student.mjthomas.website.data

import android.os.Parcelable

interface User : Parcelable {
	var name : String
	var errorCallback: (Any?) -> Unit
	var permissions: Permissions
	val key : String?
	val isLoggedIn : Boolean
	fun authError(it: Any?)
	fun cancel()
	fun login(password: String, f: (User) -> Unit)
	fun logout(f: () -> Unit)
	fun setPassword(password: String)
	fun fetch(f: (User) -> Unit)
	fun save(f: (User) -> Unit)
	fun remove(f: (User) -> Unit)
	companion object {
		const val PARCEL = "edu.fullsail.student.mjthomas.website.user"
	}
}
