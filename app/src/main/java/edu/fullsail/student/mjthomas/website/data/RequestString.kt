package edu.fullsail.student.mjthomas.website.data

import android.util.Base64
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import org.json.JSONObject
import java.nio.charset.Charset

class RequestString(
	method: Int,
	url: String,
	private val data: JSONObject?,
	private val str: String?,
	cb: (response: String) -> Unit,
	err: (error: VolleyError) -> Unit,
	private val user: String? = null,
	private val key: String? = null
) : StringRequest(
	method,
	url,
	cb,
	err
), Request {
	override fun getBody(): ByteArray =
		data?.toString()?.toByteArray() ?:
			str?.toByteArray() ?: super.getBody()
	override fun getHeaders(): MutableMap<String, String> =
		when (user != null && key != null) {
			true -> mutableMapOf(
				Pair(
					"Authorization",
					"Basic ${Base64.encode("$user:$key".toByteArray(), Base64.DEFAULT).toString(Charset.forName("UTF-8"))}"
				)
			)
			else -> super.getHeaders()
		}
}
