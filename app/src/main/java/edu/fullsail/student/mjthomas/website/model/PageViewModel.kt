package edu.fullsail.student.mjthomas.website.model

import androidx.lifecycle.ViewModel

class PageViewModel : ViewModel() {
	fun getPage(name: String) = LivePage(name).also {
		it.fetch {}
	}
}
