package edu.fullsail.student.mjthomas.website.ui

enum class Action {
	EDIT, NEW, DELETE;
	companion object {
		const val PARCEL = "edu.fullsail.student.mjthomas.website.action"
	}
}
