package edu.fullsail.student.mjthomas.website.data

import android.util.Base64
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject
import java.nio.charset.Charset

class RequestJsonObject(
	method: Int,
	url: String,
	data: JSONObject?,
	cb: (response: JSONObject) -> Unit,
	err: (error: VolleyError) -> Unit,
	private val user: String? = null,
	private val key: String? = null
) : JsonObjectRequest(
	method,
	url,
	data,
	cb,
	err
), Request {
	override fun getHeaders(): MutableMap<String, String> =
		when (user != null && key != null) {
			true -> mutableMapOf(
				Pair(
					"Authorization",
					"Basic ${Base64.encode("$user:$key".toByteArray(), Base64.DEFAULT).toString(Charset.forName("UTF-8"))}"
				)
			)
			else -> super.getHeaders()
		}
}
