package edu.fullsail.student.mjthomas.website.model

import android.os.Parcel
import android.os.Parcelable
import androidx.lifecycle.LiveData
import edu.fullsail.student.mjthomas.website.data.HttpPage
import edu.fullsail.student.mjthomas.website.data.Page

class LivePage(
	private val page : Page
) :
	Page by page,
	LiveData<Page>()
{
	init {
		value = page
	}
	constructor(
		name: String,
		body: String = ""
	) : this(HttpPage(name, body))
	override var body: String
		get() = page.body
		set(body) {
			page.body = body
			value = page
		}
	override fun fetch(f: (Page) -> Unit) = page.fetch {
		value = it
		f(this)
	}
	override fun save(f: (Page) -> Unit) = page.save {
		value = it
		f(this)
	}
	override fun remove(f: () -> Unit) = page.remove {
		value = page
		f()
	}
	companion object CREATOR : Parcelable.Creator<Page> {
		override fun createFromParcel(source: Parcel): Page = HttpPage.createFromParcel(source)
		override fun newArray(size: Int): Array<Page?> = HttpPage.newArray(size)
	}
}
