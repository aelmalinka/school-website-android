package edu.fullsail.student.mjthomas.website.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.data.RequestFactory

class SetServerIP : AppCompatActivity() {
	private val serverIP : String
		get() = findViewById<EditText>(R.id.server_ip).text.toString()
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_set_server_ip)

		findViewById<Button>(R.id.cancel).setOnClickListener {
			finish()
		}
		findViewById<Button>(R.id.save).setOnClickListener {
			RequestFactory.baseUrl = "http://${serverIP.replace(Regex("^http://"), "")}"
			finish()
		}
	}
}
