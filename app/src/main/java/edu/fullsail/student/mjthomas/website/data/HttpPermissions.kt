package edu.fullsail.student.mjthomas.website.data

import android.util.Log
import com.android.volley.VolleyError
import org.json.JSONObject

class HttpPermissions : Permissions {
	private val map : MutableMap<String, MutableMap<String, Boolean>> = mutableMapOf()
	private var modified = false
	override var errorCallback: (Any?) -> Unit = {}
	override val isModified: Boolean
		get() = modified
	override fun cancel() {
		fetch?.cancel()
	}
	override fun fetch(f: (Permissions) -> Unit) {
		fetch?.cancel()
		fetch = factory.getJson("", {
			parsePermissions(map, it)
			f(this)
		}, {
			error(it)
		})
	}
	override fun set(app: String, name: String, value: Boolean) {
		modified = true
		when(map[app]) {
			null -> map[app] = mutableMapOf(Pair(name, value))
			else -> map[app]!![name] = value
		}
	}
	private var fetch : Request? = null
	companion object {
		fun allPermissions(f: (Map<String, List<String>>) -> Unit) {
			factory.getJson("all", {
				val ret = mutableMapOf<String, List<String>>()
				for(i in it.keys()) {
					val v = mutableListOf<String>()
					for(j in 0 until it.getJSONArray(i).length()) {
						v.add(j, it.getJSONArray(i)[j] as String)
					}
					ret[i] = v
				}
				f(ret)
			}, {
				error(it)
			}, false)
		}
		fun defaultPermissions(f: (Map<String, Map<String, Boolean>>) -> Unit) {
			factory.getJson("default", {
				val ret = mutableMapOf<String, MutableMap<String, Boolean>>()
				parsePermissions(ret, it)
				f(ret)
			}, {
				error(it)
			}, false)
		}
		private fun parsePermissions(perms: MutableMap<String, MutableMap<String, Boolean>>, it: JSONObject) {
			for(i in it.getJSONObject("permissions").keys()) {
				for(j in it.getJSONObject("permissions").getJSONObject(i).keys()) {
					if(!perms.contains(i))
						perms[i] = mutableMapOf()
					perms[i]!![j] = it.getJSONObject("permissions").getJSONObject(i).getBoolean(j)
				}
			}
		}
		var error: (Any?) -> Unit = {}
		var factory : RequestFactory = RequestFactory("8082")
		private const val TAG = "Website/Permissions"
	}
	private fun error(it: Any?) {
		when(it) {
			is Error -> Log.e(TAG, it.message, it)
			is VolleyError -> Log.e(TAG, it.message, it)
			else -> Log.e(TAG, "$it")
		}
		errorCallback(it)
	}
	override val entries: Set<Map.Entry<String, Map<String, Boolean>>>
		get() = map.entries
	override val values: Collection<Map<String, Boolean>>
		get() = map.values
	override val keys: Set<String>
		get() = map.keys
	override val size: Int
		get() = map.size
	override fun containsKey(key: String): Boolean = map.containsKey(key)
	override fun containsValue(value: Map<String, Boolean>): Boolean = map.containsValue(value)
	override fun get(key: String): Map<String, Boolean>? = map[key]
	override fun isEmpty(): Boolean = map.isEmpty()
}
