package edu.fullsail.student.mjthomas.website.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.data.User
import edu.fullsail.student.mjthomas.website.ui.SelectUser.OnUserSelectedListener
import kotlinx.android.synthetic.main.list_item_page_user.view.*

class SelectUserAdapter(
	private val mValues: List<User>,
	private val mListener: OnUserSelectedListener?
) : RecyclerView.Adapter<SelectUserAdapter.ViewHolder>() {
	private val mOnClickListener: View.OnClickListener
	init {
		mOnClickListener = View.OnClickListener { v ->
			val item = v.tag as User
			mListener?.onUserSelected(item)
		}
	}
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val view = LayoutInflater.from(parent.context)
			.inflate(R.layout.list_item_page_user, parent, false)
		return ViewHolder(view)
	}
	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val item = mValues[position]
		holder.mIdView.text = item.name
		with(holder.mView) {
			tag = item
			setOnClickListener(mOnClickListener)
		}
	}
	override fun getItemCount(): Int = mValues.size
	inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
		val mIdView: TextView = mView.item
	}
}
