package edu.fullsail.student.mjthomas.website.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.android.volley.VolleyError
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.data.User
import edu.fullsail.student.mjthomas.website.model.UserViewModel

class LoginLogoutActivity :
	AppCompatActivity(),
	Login.OnLoggedIn,
	Logout.OnLoggedOut
{
	private val model : UserViewModel
		get() = ViewModelProviders.of(this).get(UserViewModel::class.java)
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_login_logout)

		val user : User? = intent.getParcelableExtra(User.PARCEL)
		if(user != null)
			model.setUser(intent.getParcelableExtra(User.PARCEL))

		model.errorCallback = {
			when(it) {
				is Error -> Toast.makeText(this, "Error: ${it.message}", Toast.LENGTH_SHORT).show()
				is VolleyError -> Toast.makeText(this, "Volley Error: $it", Toast.LENGTH_SHORT).show()
				is String -> Toast.makeText(this, "Error: $it", Toast.LENGTH_SHORT).show()
				else -> Toast.makeText(this, "Unknown Error", Toast.LENGTH_SHORT).show()
			}
			setResult(RESULT_OK)
			finish()
		}

		when(model.isLoggedIn) {
			true -> logout()
			else -> login()
		}
	}
	private fun login() =
		with(supportFragmentManager.beginTransaction()) {
			val frag = Login.newInstance()
			add(R.id.login_main, frag)
			commit()
		}
	private fun logout() =
		with(supportFragmentManager.beginTransaction()) {
			val frag = Logout.newInstance()
			add(R.id.login_main, frag)
			commit()
		}
	override fun onLoggedIn() {
		Toast.makeText(this, "Logged In", Toast.LENGTH_SHORT).show()
		setResult(RESULT_OK, Intent(this, MainActivity::class.java).apply {
			putExtra(User.PARCEL, model.user.value)
		})
		finish()
	}
	override fun onLoggedOut() {
		Toast.makeText(this, "Logged Out", Toast.LENGTH_SHORT).show()
		setResult(RESULT_OK)
		finish()
	}
	override fun onCancel() {
		model.cancel()
		setResult(RESULT_CANCELED)
		finish()
	}
}
