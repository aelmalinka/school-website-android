package edu.fullsail.student.mjthomas.website.ui

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.VolleyError
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.data.User

class EditUserActivity :
	AppCompatActivity(),
	SelectUser.OnUserSelectedListener,
	EditUser.OnUserChangedListener
{
	private var action: Action? = null
	private var myUser: User? = null
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_edit_user)

		action = intent.getSerializableExtra(Action.PARCEL) as Action
		myUser = intent.getParcelableExtra(User.PARCEL) as User?

		SelectUser.errorCallback = {
			onError(it)
		}
		myUser?.errorCallback = SelectUser.errorCallback

		when(action) {
			Action.NEW -> new()
			Action.EDIT -> select()
			Action.DELETE -> select()
			else -> Log.e(TAG, "Invalid Action")
		}
	}
	private fun new() =
		with(supportFragmentManager.beginTransaction()) {
			add(R.id.user_edit_main, EditUser.newInstance())
			commit()
		}
	private fun select() {
		when (myUser) {
			null -> select(false)
			else -> myUser?.fetch {
				select(it.permissions["users"]?.get("list") ?: false)
			}
		}
	}
	private fun select(canList: Boolean) =
		with(supportFragmentManager.beginTransaction()) {
			add(R.id.user_edit_main, SelectUser.newInstance(canList))
			commit()
		}
	override fun onUserChanged(user: User) {
		Toast.makeText(
			this,
			when(action) {
				Action.NEW -> "${user.name} Added!"
				Action.EDIT -> "${user.name} Edited!"
				else -> "${user.name} ${action}ed!"
			},
			Toast.LENGTH_SHORT
		).show()
		finish()
	}
	override fun onUserSelected(user: User) {
		when (action) {
			Action.EDIT -> edit(user)
			Action.DELETE -> delete(user)
			else ->
				Log.e(TAG, "User Selected with invalid action")
		}
	}
	override fun onCancel() = finish()
	override fun onError(it: Any?) {
		when(it) {
			is Error -> Toast.makeText(this, "Error: ${it.message}", Toast.LENGTH_SHORT).show()
			is VolleyError -> Toast.makeText(this, "Volley Error: $it", Toast.LENGTH_SHORT).show()
			is String -> Toast.makeText(this, "Error: $it", Toast.LENGTH_SHORT).show()
			else -> Toast.makeText(this, "Unknown Error", Toast.LENGTH_SHORT).show()
		}
		finish()
	}
	private fun edit(user: User) =
		with(supportFragmentManager.beginTransaction()) {
			replace(R.id.user_edit_main, EditUser.newInstance(user))
			commit()
		}
	private fun delete(user: User) =
		user.remove {
			Toast.makeText(this, "${user.name} Deleted!", Toast.LENGTH_SHORT).show()
			finish()
		}
	companion object {
		const val TAG = "Website/EditUserActivity"
	}
}
