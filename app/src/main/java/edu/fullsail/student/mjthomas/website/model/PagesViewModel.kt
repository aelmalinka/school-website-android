package edu.fullsail.student.mjthomas.website.model

import androidx.lifecycle.ViewModel

class PagesViewModel : ViewModel() {
	val pages : LivePages by lazy {
		LivePages().also {
			it.fetch {}
		}
	}
}
