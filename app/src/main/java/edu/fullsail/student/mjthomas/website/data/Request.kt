package edu.fullsail.student.mjthomas.website.data

interface Request {
	fun cancel()
}
