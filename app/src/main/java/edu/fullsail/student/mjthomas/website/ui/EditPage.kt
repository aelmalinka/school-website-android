package edu.fullsail.student.mjthomas.website.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.data.Page

class EditPage : Fragment() {
	private var page: Page? = null
	private var listener: OnPageEdited? = null
	private val editText: TextView?
		get() = view?.findViewById(R.id.editText)
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		arguments?.let {
			page = it.getParcelable(Page.PARCEL)
		}
	}
	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		val ret = inflater.inflate(R.layout.fragment_edit_page, container, false)

		with(ret) {
			val text: TextView = findViewById(R.id.editText)
			page?.fetch {
				text.text = it.body

				val cancel = findViewById<Button>(R.id.editCancel)
				val save = findViewById<Button>(R.id.editSave)

				cancel.setOnClickListener {
					onCancel()
				}
				save.setOnClickListener {
					onSave()
				}
			}
		}

		return ret
	}
	override fun onAttach(context: Context) {
		super.onAttach(context)
		if (context is OnPageEdited) {
			listener = context
		} else {
			throw RuntimeException("$context must implement OnFragmentInteractionListener")
		}
	}
	override fun onDetach() {
		super.onDetach()
		listener = null
	}
	interface OnPageEdited {
		fun onPageEdited(page: Page)
		fun onCancel()
	}
	private fun onCancel() {
		listener?.onCancel()
	}
	private fun onSave() {
		page?.body = editText?.text.toString()
		page?.save {
			listener?.onPageEdited(it)
		}
	}
	companion object {
		@JvmStatic
		fun newInstance(page: Page) =
			EditPage().apply {
				arguments = Bundle().apply {
					putParcelable(Page.PARCEL, page)
				}
			}
	}
}
