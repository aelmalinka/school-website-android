package edu.fullsail.student.mjthomas.website.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import edu.fullsail.student.mjthomas.website.R
import edu.fullsail.student.mjthomas.website.data.Page
import edu.fullsail.student.mjthomas.website.data.Pages

class SelectPage : Fragment() {
	private var pages: Pages? = null
	private var listener: OnPageSelected? = null
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		arguments?.let {
			pages = it.getParcelable(PARCEL_PAGES)
		}
		pages?.errorCallback = errorCallback
	}
	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? =
		inflater.inflate(R.layout.fragment_page_list, container, false).apply {
			pages?.fetch {
				with(findViewById<RecyclerView>(R.id.list)) {
					layoutManager = LinearLayoutManager(context)
					adapter = SelectPageAdapter(it, listener)
				}
			}
			findViewById<Button>(R.id.cancel).setOnClickListener {
				listener?.onCancel()
			}
		}
	override fun onAttach(context: Context) {
		super.onAttach(context)
		if (context is OnPageSelected) {
			listener = context
		} else {
			throw RuntimeException("$context must implement OnListFragmentInteractionListener")
		}
	}
	override fun onDetach() {
		super.onDetach()
		listener = null
	}
	interface OnPageSelected {
		fun onPageSelected(page: Page)
		fun onCancel()
	}
	companion object {
		const val PARCEL_PAGES = "edu.fullsail.student.mjthomas.website.pages"
		var errorCallback: (Any?) -> Unit = {}
		@JvmStatic
		fun newInstance(pages: Pages) =
			SelectPage().apply {
				arguments = Bundle().apply {
					putParcelable(PARCEL_PAGES, pages)
				}
			}
	}
}
