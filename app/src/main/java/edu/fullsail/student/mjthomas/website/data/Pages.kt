package edu.fullsail.student.mjthomas.website.data

import android.os.Parcelable

interface Pages : List<Page>, Parcelable {
	var errorCallback: (Any?) -> Unit
	fun cancel()
	fun fetch(f: (Pages) -> Unit)
	fun add(page: Page, f: (Page) -> Unit)
	companion object {
		const val PARCEL = "edu.fullsail.student.mjthomas.website.pages"
	}
}
