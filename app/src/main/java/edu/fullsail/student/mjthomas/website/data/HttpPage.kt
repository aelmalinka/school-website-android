package edu.fullsail.student.mjthomas.website.data

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.android.volley.VolleyError

data class HttpPage(
	override val name: String,
	override var body: String = ""
) : Page {
	override var errorCallback: (Any?) -> Unit = {}
	override fun toString(): String = this.name
	override fun cancel() {
		fetchReq?.cancel()
		saveReq?.cancel()
		delReq?.cancel()
	}
	override fun fetch(f: (Page) -> Unit) {
		fetchReq?.cancel()
		fetchReq = factory.getString(name, {
			body = it
			f(this)
		}, {
			error(it)
		})
	}
	override fun save(f: (Page) -> Unit) {
		saveReq?.cancel()
		saveReq = factory.postString(name, body, {
			f(this)
		}, {
			error(it)
		})
	}
	override fun remove(f: () -> Unit) {
		delReq?.cancel()
		delReq = factory.delete(name, {
			f()
		}, {
			error(it)
		})
	}
	override fun describeContents(): Int = 0
	override fun writeToParcel(dest: Parcel?, flags: Int) {
		dest?.writeString(name)
	}
	private var fetchReq : Request? = null
	private var saveReq : Request? = null
	private var delReq : Request? = null
	companion object CREATOR : Parcelable.Creator<Page> {
		override fun createFromParcel(source: Parcel): Page {
			return HttpPage(source.readString() ?: "")
		}
		override fun newArray(size: Int): Array<Page?> {
			return arrayOfNulls(size)
		}
		var factory: RequestFactory = RequestFactory()
		const val TAG: String = "Website/Pages"
	}
	private fun error(it: Any?) {
		when(it) {
			is Error -> Log.e(TAG, it.message, it)
			is VolleyError -> Log.e(TAG, it.message, it)
			else -> Log.e(TAG, "$it")
		}
		errorCallback(it)
	}
}
