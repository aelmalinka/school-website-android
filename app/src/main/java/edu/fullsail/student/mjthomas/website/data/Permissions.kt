package edu.fullsail.student.mjthomas.website.data

interface Permissions :
	Map<String, Map<String, Boolean>>
{
	var errorCallback: (Any?) -> Unit
	val isModified : Boolean
	fun fetch(f: (Permissions) -> Unit)
	fun cancel()
	fun set(app: String, name: String, value: Boolean)
}
