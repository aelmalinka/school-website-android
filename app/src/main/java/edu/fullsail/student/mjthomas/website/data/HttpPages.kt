package edu.fullsail.student.mjthomas.website.data

import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.android.volley.VolleyError

class HttpPages : Pages {
	override var errorCallback: (Any?) -> Unit = {}
		set(value) {
			field = value
			for(i in pages)
				i.errorCallback = value
		}
	private val pages : MutableList<Page> = mutableListOf()
	private var req : Request? = null
	override fun cancel() {
		req?.cancel()
	}
	override fun fetch(f: (Pages) -> Unit) {
		req?.cancel()
		req = factory.getJsonArray("", {
			pages.clear()
			for(i in 0 until it.length())
				pages.add(HttpPage(it.getJSONObject(i).getString("name")).also { page ->
					page.errorCallback = errorCallback
				})
			f(this)
		}, {
			error(it)
		})
	}
	override fun add(page: Page, f: (Page) -> Unit) {
		req?.cancel()
		req = factory.putString(page.name, page.body, {
			f(HttpPage(it).also {page ->
				page.errorCallback = errorCallback
			})
		}, {
			error(it)
		})
	}
	override fun describeContents(): Int = 0
	override fun writeToParcel(dest: Parcel, @Suppress("UNUSED_PARAMETER")flags: Int) {
		dest.writeStringList(pages.map { it.name })
	}
	companion object CREATOR : Parcelable.Creator<Pages> {
		fun get(f: (Pages) -> Unit) {
			return HttpPages().fetch(f)
		}
		override fun createFromParcel(source: Parcel): Pages {
			val pages = mutableListOf<String>()
			source.readStringList(pages)
			val ret = HttpPages()
			pages.forEach { ret.pages.add(HttpPage(it)) }
			return ret
		}
		override fun newArray(size: Int): Array<Pages?> {
			return arrayOfNulls(size)
		}
		var factory: RequestFactory = RequestFactory()
		const val TAG = "Website/Pages"
	}
	private fun error(it: Any?) {
		when(it) {
			is Error -> Log.e(TAG, it.message, it)
			is VolleyError -> Log.e(TAG, it.message, it)
			else -> Log.e(TAG, "$it")
		}
		errorCallback(it)
	}
	override val size : Int get() = pages.size
	override fun get(index: Int): Page = pages[index]
	override fun isEmpty(): Boolean = pages.isEmpty()
	override fun iterator(): Iterator<Page> = pages.iterator()
	override fun listIterator(): ListIterator<Page> = pages.listIterator()
	override fun listIterator(index: Int): ListIterator<Page> = pages.listIterator(index)
	override fun subList(fromIndex: Int, toIndex: Int): List<Page> = pages.subList(fromIndex, toIndex)
	// 2019-06-05 AMR NOTE: contains*/*indexOf are useless because HttpPages aren't comparable
	override fun contains(element: Page): Boolean = pages.contains(element)
	override fun containsAll(elements: Collection<Page>): Boolean = pages.containsAll(elements)
	override fun indexOf(element: Page): Int = pages.indexOf(element)
	override fun lastIndexOf(element: Page): Int = pages.lastIndexOf(element)
}
