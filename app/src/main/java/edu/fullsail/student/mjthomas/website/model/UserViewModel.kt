package edu.fullsail.student.mjthomas.website.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import edu.fullsail.student.mjthomas.website.data.HttpUser
import edu.fullsail.student.mjthomas.website.data.User

class UserViewModel : ViewModel() {
	private val _user: MutableLiveData<User> by lazy {
		MutableLiveData<User>().apply {
			value = null
		}
	}
	val user : LiveData<User>
		get() = _user
	fun setUser(user: User?) {
		_user.value = user
		_user.value?.errorCallback = errorCallback
	}
	val isLoggedIn : Boolean
		get() = user.value?.isLoggedIn ?: false
	var errorCallback: (Any?) -> Unit = {}
		set(value) {
			field = value
			user.value?.errorCallback = value
		}
	fun cancel() {
		user.value?.cancel()
	}
	fun login(name: String, password: String, f: (User) -> Unit) {
		logout {
			user.value?.cancel()
			_user.value = HttpUser(name).apply {
				errorCallback = this@UserViewModel.errorCallback
				login(password, f)
			}
		}
	}
	fun logout(f: () -> Unit) {
		user.value?.cancel()
		user.value?.logout {
			_user.value = null
			f()
		} ?: f()
	}
}
